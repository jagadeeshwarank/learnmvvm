package in.flashbulb.learnmvvm.io.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import in.flashbulb.learnmvvm.models.Movie;

public class MovieListResponse {

    @SerializedName("results")
    private List<Movie> results;
    @SerializedName("page")
    private int page;
    @SerializedName("total_results")
    private int totalResults;
    @SerializedName("total_pages")
    private int totalPages;

    public int getPage() {
        return page;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public List<Movie> getResults() {
        return results;
    }
}
