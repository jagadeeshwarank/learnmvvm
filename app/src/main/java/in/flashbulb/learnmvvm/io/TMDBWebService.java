package in.flashbulb.learnmvvm.io;

import in.flashbulb.learnmvvm.io.response.MovieListResponse;
import in.flashbulb.learnmvvm.models.MovieDetail;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TMDBWebService {

    @GET("movie/popular")
    Call<MovieListResponse> getPopularMovies();

    @GET("movie/{movieId}")
    Call<MovieDetail> getMovieDetails(@Path("movieId") int movieId);

}
