package in.flashbulb.learnmvvm.io;

import android.support.annotation.NonNull;

import java.io.IOException;

import in.flashbulb.learnmvvm.BuildConfig;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class HttpClientFactory {

    //originally from https://futurestud.io/tutorials/retrofit-2-how-to-add-query-parameters-to-every-request
    public static OkHttpClient getTMDBClient() {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.addInterceptor(getLoggingInterceptor());
        httpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request originalRequest = chain.request();
                HttpUrl originalUrl = originalRequest.url();

                HttpUrl modUrl = originalUrl.newBuilder()
                        .addQueryParameter("api_key", BuildConfig.API_KEY)
                        .build();
                Request.Builder requestBuilder = originalRequest.newBuilder()
                        .url(modUrl);

                Request modRequest = requestBuilder.build();
                return chain.proceed(modRequest);
            }
        });

        return httpClientBuilder.build();
    }

    @NonNull
    private static HttpLoggingInterceptor getLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }
}
