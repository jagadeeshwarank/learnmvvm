package in.flashbulb.learnmvvm;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.flashbulb.learnmvvm.models.Movie;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private List<Movie> movies;

    public MovieAdapter() {

    }

    public void setMovies(List<Movie> movieList) {
        this.movies = movieList;
    }

    public Movie getMovieAt(int position) {
        if (movies != null) {
            return movies.get(position);
        }
        return null;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView tvMovie = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_movie, parent, false);

        return new ViewHolder(tvMovie);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textView.setText(movies.get(position).getOriginalTitle());
    }

    @Override
    public int getItemCount() {
        if (movies != null) {
            return movies.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public ViewHolder(TextView itemView) {
            super(itemView);
            this.textView = itemView;
        }
    }
}
