package in.flashbulb.learnmvvm;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.List;

import in.flashbulb.learnmvvm.io.response.MovieListResponse;
import in.flashbulb.learnmvvm.models.Movie;
import in.flashbulb.learnmvvm.models.MovieDetail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieRepository {

    public LiveData<MovieDetail> getMovieDetails(int movieId) {
        final MutableLiveData<MovieDetail> data = new MutableLiveData<>();

        //TODO: add room persistence library later
        App.getInstance().getTmdbWebService().getMovieDetails(movieId).enqueue(new Callback<MovieDetail>() {
            @Override
            public void onResponse(@NonNull Call<MovieDetail> call, @NonNull Response<MovieDetail> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<MovieDetail> call, @NonNull Throwable t) {
                //TODO: handle failure gracefully
            }
        });
        return data;
    }

    public LiveData<List<Movie>> getPopularMovies() {
        //TODO: refactor to include paging
        final MutableLiveData<List<Movie>> data = new MutableLiveData<>();
        App.getInstance().getTmdbWebService().getPopularMovies().enqueue(new Callback<MovieListResponse>() {
            @Override
            public void onResponse(@NonNull Call<MovieListResponse> call, @NonNull Response<MovieListResponse> response) {
                if (response.body() != null) {
                    data.setValue(response.body().getResults());
                }
            }

            @Override
            public void onFailure(@NonNull Call<MovieListResponse> call, @NonNull Throwable t) {
                //TODO: handle failure gracefully
            }
        });
        return data;

    }
}
