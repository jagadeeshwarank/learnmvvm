package in.flashbulb.learnmvvm;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import in.flashbulb.learnmvvm.models.Movie;
import in.flashbulb.learnmvvm.utils.ItemClickSupport;
import in.flashbulb.learnmvvm.viewmodels.MovieListViewModel;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvMovieList;
    private MovieRepository movieRepository;
    private MovieListViewModel viewModel;
    private MovieAdapter movieAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvMovieList = findViewById(R.id.rvMovies);
        rvMovieList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        movieRepository = new MovieRepository();

        movieAdapter = new MovieAdapter();
        rvMovieList.setAdapter(movieAdapter);

        viewModel = new MovieListViewModel(movieRepository);
        observeViewModel();

        ItemClickSupport.addTo(rvMovieList).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Movie selectedMovie = movieAdapter.getMovieAt(position);
                Intent detailedMovie = new Intent(MainActivity.this, MovieDetailActivity.class);
                detailedMovie.putExtra("movieId", selectedMovie.getId());
                startActivity(detailedMovie);
            }
        });


    }

    private void observeViewModel() {
        viewModel.getPopularMoviesList().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(@Nullable List<Movie> movieList) {
                if (movieList != null) {
                    movieAdapter.setMovies(movieList);
                    movieAdapter.notifyDataSetChanged();
                }
            }
        });
    }


}
