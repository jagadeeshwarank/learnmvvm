package in.flashbulb.learnmvvm;


import in.flashbulb.learnmvvm.io.HttpClientFactory;
import in.flashbulb.learnmvvm.io.TMDBWebService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends android.app.Application {

    private static App instance;
    private TMDBWebService tmdbWebService;

    public static App getInstance() {
        return instance;
    }

    //TODO introduce dagger at later stage
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initWebServices();
    }

    private void initWebServices() {
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(HttpClientFactory.getTMDBClient());
        tmdbWebService = retrofitBuilder.build().create(TMDBWebService.class);
    }

    public TMDBWebService getTmdbWebService() {
        return tmdbWebService;
    }

}
