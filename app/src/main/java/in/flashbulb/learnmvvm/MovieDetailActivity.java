package in.flashbulb.learnmvvm;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import in.flashbulb.learnmvvm.models.MovieDetail;
import in.flashbulb.learnmvvm.viewmodels.MovieDetailViewModel;

public class MovieDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        final TextView tvDetails = findViewById(R.id.txtMovieDetail);

        int movieId = getIntent().getIntExtra("movieId", -1);
        MovieRepository movieRepository = new MovieRepository();

        MovieDetailViewModel detailViewModel = new MovieDetailViewModel(movieRepository);
        detailViewModel.init(movieId);
        detailViewModel.getMovieDetail().observe(this, new Observer<MovieDetail>() {
            @Override
            public void onChanged(@Nullable MovieDetail movieDetail) {
                tvDetails.setText(movieDetail.toString());
            }
        });
//        movieRepository.getMovieDetails();
    }
}
