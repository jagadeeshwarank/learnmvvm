package in.flashbulb.learnmvvm.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import in.flashbulb.learnmvvm.MovieRepository;
import in.flashbulb.learnmvvm.models.MovieDetail;

public class MovieDetailViewModel extends ViewModel {
    private LiveData<MovieDetail> movieDetail;
    private MovieRepository movieRepository;

    public MovieDetailViewModel(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public void init(int movieId) {
        movieDetail = movieRepository.getMovieDetails(movieId);
    }

    public LiveData<MovieDetail> getMovieDetail() {

        return movieDetail;
    }

}
