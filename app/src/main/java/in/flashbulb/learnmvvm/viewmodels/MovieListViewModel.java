package in.flashbulb.learnmvvm.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import in.flashbulb.learnmvvm.MovieRepository;
import in.flashbulb.learnmvvm.models.Movie;

public class MovieListViewModel extends ViewModel {
    private MovieRepository movieRepository;
    private LiveData<List<Movie>> moviesList;

    public MovieListViewModel(MovieRepository repository) {
        this.movieRepository = repository;
        moviesList = movieRepository.getPopularMovies();
    }

    public void init(int pageNo) {
        moviesList = movieRepository.getPopularMovies();
    }

    public LiveData<List<Movie>> getPopularMoviesList() {
        return moviesList;
    }
}
